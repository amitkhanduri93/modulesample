import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContractSignatureFormComponent } from './contract-signature-form.component';
import { By } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppMaterialModule } from '../../../../shared/app-material.module';
import { CardComponent } from '../../../../shared/components/card/card.component';
import { ContractSignatureServiceMock } from '../../../../core/mocks/contractSignature.service.mock';
import { ContractSignatureService } from '../../../../core/http/contract-signature.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ContractSignedInformationComponent } from '../contract-signed-information/contract-signed-information.component';
import { CurrencyFormat } from '../../../../shared/pipes/currency.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ContractSignatureFormComponent Integration', () => {
  let component: ContractSignatureFormComponent;
  let fixture: ComponentFixture<ContractSignatureFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContractSignatureFormComponent, ContractSignedInformationComponent, CardComponent, CurrencyFormat],
      imports: [BrowserAnimationsModule, ReactiveFormsModule, AppMaterialModule, HttpClientTestingModule],
      providers: [ContractSignatureServiceMock, ContractSignatureService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    const service = new ContractSignatureServiceMock();
    fixture = TestBed.createComponent(ContractSignatureFormComponent);
    component = fixture.componentInstance;
    component.personalGuarantee = service.personalGuaranteeTestData;
    component.signer = service.signerTestData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have change address Form', () => {
    const de = fixture.debugElement.query(By.css('form'));
    const el: HTMLElement = de.nativeElement;
    expect(el).not.toBeNull();
  });

  it('should have Form fields', () => {
    const formFields = fixture.debugElement.queryAll(By.css('input'));

    const nameField: HTMLElement = formFields[0].nativeElement;
    expect(nameField).not.toBeNull();

    const telephoneNumberField: HTMLElement = formFields[1].nativeElement;
    expect(telephoneNumberField).not.toBeNull();

    const streetField: HTMLElement = formFields[2].nativeElement;
    expect(streetField).not.toBeNull();

    const nifField: HTMLElement = formFields[3].nativeElement;
    expect(nifField).not.toBeNull();

    const postalCodeField: HTMLElement = formFields[4].nativeElement;
    expect(postalCodeField).not.toBeNull();

    const cityField: HTMLElement = formFields[5].nativeElement;
    expect(cityField).not.toBeNull();

  });

  it('should have submit and cancel button', () => {
    const compareTextConfirm = 'Aceitar';
    const compareTextCancel = 'Declinar';
    fixture.detectChanges();
    const formButtons = fixture.debugElement.queryAll(By.css('button'));

    const confirmButton: HTMLElement = formButtons[0].nativeElement;
    expect(confirmButton).not.toBeNull();
    expect(confirmButton.innerText.toLowerCase()).toContain(compareTextConfirm.toLowerCase());

    const cancelButton: HTMLElement = formButtons[1].nativeElement;
    expect(cancelButton).not.toBeNull();
    expect(cancelButton.innerText.toLowerCase()).toContain(compareTextCancel.toLowerCase());
  });

  it('submit button should be disabled when required field values are not provided', () => {
    component.signatureForm.controls['name'].setValue('');
    component.signatureForm.controls['telephoneNumber'].setValue('');
    component.signatureForm.controls['street'].setValue('');
    component.signatureForm.controls['nif'].setValue('');
    component.signatureForm.controls['postalCode'].setValue('');
    component.signatureForm.controls['city'].setValue('');
    fixture.detectChanges();
    const submitButton = fixture.debugElement.queryAll(By.css('button'))[0].nativeElement;
    expect(submitButton.hasAttribute('disabled')).toBe(true);
  });

  it('when addClauseInformation is called contact should be defined', () => {
    component.signatureForm.controls['name'].setValue('test');
    component.signatureForm.controls['telephoneNumber'].setValue('100203121');
    component.signatureForm.controls['street'].setValue('211');
    component.signatureForm.controls['nif'].setValue('122300123');
    component.signatureForm.controls['postalCode'].setValue('1001-123');
    component.signatureForm.controls['city'].setValue('test');
    component.form = fixture.componentInstance.form;
    fixture.detectChanges();
    component.addClauseInformation(component.signatureForm);
    expect(component.contact).toBeDefined();
    expect(component.isLoading).toBeFalsy();
  });

});
