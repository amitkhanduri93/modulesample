import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import {
  ContractSignatureCancellationModalComponent
} from './contract-signature-cancellation-modal/contract-signature-cancellation-modal.component';
import { Sign, PersonalGuarantee, Signer } from '../../../../shared/models/contract-signature';
import { CustomValidators } from '../../../../shared/directives/custom-validators';
import { ContractSignatureService } from '../../../../core/http/contract-signature.service';
import { MatSnackBar } from '@angular/material';
import { AlertBarComponent } from '../../../../shared/components/alert-bar/alert-bar.component';
import { LoadingService } from '../../../../core/services/loading.service';

@Component({
  selector: 'app-contract-signature-form',
  templateUrl: './contract-signature-form.component.html',
  styleUrls: ['./contract-signature-form.component.scss']
})
export class ContractSignatureFormComponent implements OnInit, OnDestroy {

  @ViewChild('form') form;
  @Input() docRecipientId: string;
  @Input() declinedReasons: Array<string>;
  @Input() personalGuarantee: PersonalGuarantee;
  @Input() signer: Signer;
  isLoading: boolean;
  declaration: boolean;
  contact: Sign;
  reason = false;

  signatureForm: FormGroup;
  contractSignatureFormSubscription;
  contractSignatureChangeIbanSubscription;
  ibanDeclaration = false;

  constructor(private fb: FormBuilder, private dialog: MatDialog,
    private contractSignatureService: ContractSignatureService, private snackBar: MatSnackBar,
    private loadingService: LoadingService) {
  }

  // Checks if changeIban is defined in the contract signature service and 
  // if true then initializes the ibanDeclaration with changeIban from the contract signature service and also calls the setForm method
  ngOnInit() {
    if (this.contractSignatureService.changeIban) {
      this.contractSignatureChangeIbanSubscription = this.contractSignatureService.changeIban.subscribe(data => {
        this.ibanDeclaration = data;
      });
    }
    this.setForm();
  }

  // Initializes the signatureForm fields values with the values recieved in the @Input()
  setForm() {
    this.signatureForm = this.fb.group({
      name: [{
        value: (this.personalGuarantee.name ? this.personalGuarantee.name : this.signer.name),
        disabled: true
      }],
      telephoneNumber: [{
        value: (this.personalGuarantee.mobile ? this.personalGuarantee.mobile : (this.signer.phone ? this.signer.phone : '')),
        disabled: this.personalGuarantee.mobile || this.signer.phone
      }, [
        Validators.required,
        CustomValidators.phoneNoValidate('telephoneNumber')
      ]],
      nif: [{
        value: (this.personalGuarantee.nif ? this.personalGuarantee.nif : ''),
        disabled: this.personalGuarantee.nif
      }, [
        Validators.required,
        Validators.pattern(/[0-9]{9}/g)
      ]],
      street: [{
        value: (this.personalGuarantee.street ? this.personalGuarantee.street : ''),
        disabled: this.personalGuarantee.street
      }, Validators.required],
      postalCode: [{
        value: (this.personalGuarantee.postalCode ? this.personalGuarantee.postalCode : ''),
        disabled: this.personalGuarantee.postalCode
      }, [
        Validators.required,
        Validators.pattern(/[0-9]{4}-[0-9]{3}/g)
      ]],
      city: [{
        value: (this.personalGuarantee.city ? this.personalGuarantee.city : ''),
        disabled: this.personalGuarantee.city
      }, Validators.required]
    });
  }

  // Opens the dialog box for cancelling the contract signature
  openCancellationDialog() {
    this.dialog.open(ContractSignatureCancellationModalComponent, {
      width: '25rem',
      data: {
        docRecipientId: this.docRecipientId,
        declinedReasons: this.declinedReasons
      }
    });
  }

  // calls the set loading and addClauseInformation methods
  addClause(form: FormGroup) {
    this.setLoading();
    this.addClauseInformation(form);
  }

  // Post the signature form data for saving the form data using the postContractSignData method of contract signature service
  addClauseInformation(formData) {
    this.contact = new Sign();
    this.contact.signerPhone = formData.controls['telephoneNumber'].value;
    this.contact.signerName = formData.controls['name'].value;
    this.contact.street = formData.controls['street'].value;
    this.contact.phone = formData.controls['telephoneNumber'].value;
    this.contact.nif = formData.controls['nif'].value;
    this.contact.city = formData.controls['city'].value;
    this.contact.postalCode = formData.controls['postalCode'].value;
    this.contractSignatureFormSubscription = this.contractSignatureService.postContractSignData(this.contact,
      this.docRecipientId).subscribe((response) => {
        this.loadingService.increment(100);
        this.isLoading = false;
        this.reason = true;
        this.contractSignatureService.setSignedData({
          name: this.contact.signerName,
          reason: this.reason,
          id: this.docRecipientId,
          date: new Date()
        });
      },
        (reason) => {
          if (reason.status === 400) {
            this.snackBar.openFromComponent(AlertBarComponent, {
              data: 'Ocurreu um erro ao assinar o contrato. Por favor, tente novamente mais tarde.',
              panelClass: 'error-snackbar'
            });
          } else if (reason.status === 403) {
            this.snackBar.openFromComponent(AlertBarComponent, {
              data: 'O nome que introduziu não corresponde ao nome registado.',
              panelClass: 'error-snackbar'
            });
          } else {
            this.snackBar.openFromComponent(AlertBarComponent, {
              data: 'Ocurreu um erro ao assinar o contrato. Por favor, tente novamente mais tarde.',
              panelClass: 'error-snackbar'
            });
          }
          this.reason = false;
          this.loadingService.increment(100);
          this.isLoading = false;
        });
  }

  // Initializes isLoading to true when it is called
  setLoading() {
    this.isLoading = true;
  }

  // Provides the nif error message when it is called 
  getNifErrorMessage() {
    return this.signatureForm.controls['nif'].hasError('required') ? 'NIF é obrigatório.' :
      this.signatureForm.controls['nif'].hasError('pattern') ? 'NIF inválido.' :
        '';
  }

  // Provides the phone error message when it is called
  getPhoneErrorMessage() {
    return this.signatureForm.controls['telephoneNumber'].hasError('required') ? 'Contacto Telefónico é obrigatório' :
      this.signatureForm.controls['telephoneNumber'].hasError('invalid') ? 'Contacto Telefónico inválido.' :
        '';
  }

  // Provides the postal code error message when it is called
  getPostalCodeErrorMessage() {
    return this.signatureForm.controls['postalCode'].hasError('required') ? 'Código Postal é obrigatório.' :
      this.signatureForm.controls['postalCode'].hasError('pattern') ? 'Código Postal inválido.' :
        '';
  }

  // Unsubscribes all the subscriptions made in the component
  ngOnDestroy() {
    this.contact = null;
    if (this.contractSignatureFormSubscription) {
      this.contractSignatureFormSubscription.unsubscribe();
    }
    if (this.contractSignatureChangeIbanSubscription) {
      this.contractSignatureChangeIbanSubscription.unsubscribe();
    }
  }

}
