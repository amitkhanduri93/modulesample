import { ContractSignatureFormComponent } from './contract-signature-form.component';
import { FormBuilder } from '@angular/forms';
import { throwError } from 'rxjs';
import { ContractSignatureServiceMock } from '../../../../core/mocks/contractSignature.service.mock';
import { LoadingService } from '../../../../core/services/loading.service';
import { MatSnackBar } from '@angular/material';

describe('ContractSignatureFormComponent', () => {
  let component: ContractSignatureFormComponent;
  let formBuilder: FormBuilder;
  let dialog;
  let service;
  let loadingService;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    dialog = jasmine.createSpyObj('dialog', ['open']);
    snackBar = jasmine.createSpyObj('snackBar', ['openFromComponent']);
    formBuilder = new FormBuilder();
    service = new ContractSignatureServiceMock();
    loadingService = new LoadingService();
    component = new ContractSignatureFormComponent(formBuilder, dialog, service, snackBar, loadingService);
    component.personalGuarantee = service.personalGuaranteeTestData;
    component.ngOnInit();
    component.signatureForm.controls['name'].setValue('test');
    component.signatureForm.controls['telephoneNumber'].setValue('120-120');
    component.signatureForm.controls['street'].setValue('211');
    component.signatureForm.controls['nif'].setValue('123123');
    component.signatureForm.controls['postalCode'].setValue('123123');
    component.signatureForm.controls['city'].setValue('test');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('signatureForm values are set when ngOnInit is called', () => {
    expect(component.signatureForm.controls['name']).toBeDefined();
    expect(component.signatureForm.controls['telephoneNumber']).toBeDefined();
    expect(component.signatureForm.controls['street']).toBeDefined();
    expect(component.signatureForm.controls['nif']).toBeDefined();
    expect(component.signatureForm.controls['postalCode']).toBeDefined();
    expect(component.signatureForm.controls['city']).toBeDefined();
  });

  it('dialog box should be closed when closeModal is called  ', () => {
    component.openCancellationDialog();
    expect(dialog.open).toHaveBeenCalled();
  });

  it('when addClause is called isLoading should be true ', () => {
    component.ngOnInit();
    spyOn(component, 'setLoading');
    component.addClause(component.signatureForm);
    expect(component.setLoading).toHaveBeenCalled();
  });

  it('isLoading should be false when addClause is called successfully', () => {
    component.ngOnInit();
    component.addClause(component.signatureForm);
    expect(component.isLoading).toBeFalsy();
  });

  it('contact information will not be added and isLoading should be false, when addClause service method is called with error', () => {
    spyOn(service, 'addClause').and.returnValue(throwError(new Error('Error: Failed to fetch data!')));
    component.addClauseInformation(component.signatureForm);
    expect(component.isLoading).toBeFalsy();
  });

  it('contact should be null, when ngOnDestroy is called', () => {
    component.contractSignatureChangeIbanSubscription = jasmine.createSpyObj('component.contractSignatureChangeIbanSubscription',
      ['unsubscribe']);
    component.contractSignatureFormSubscription = jasmine.createSpyObj('component.contractSignatureFormSubscription', ['unsubscribe']);
    component.ngOnDestroy();
    expect(component.contact).toBeNull();
    expect(component.contractSignatureChangeIbanSubscription.unsubscribe).toHaveBeenCalled();
    expect(component.contractSignatureFormSubscription.unsubscribe).toHaveBeenCalled();
  });

});
