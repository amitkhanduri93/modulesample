import { Component, Input, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointState, BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { PersonalGuarantee, Signer } from '../../../../shared/models/contract-signature';
@Component({
  selector: 'app-contract-clauses',
  templateUrl: './contract-clauses.component.html',
  styleUrls: ['./contract-clauses.component.scss']
})
export class ContractClausesComponent implements OnDestroy {

  @Input() docRecipientId: string;
  @Input() declinedReasons: Array<string>;
  @Input() personalGuarantee: PersonalGuarantee;
  @Input() signer: Signer;
  contractClauses = [{
    heading: '1. Bens objeto do Contrato. Escolha dos Bens e do Fornecedor. Cláusulas contratuais',
    clauses: [
        '1.1. O(s) bem(bens) objeto do presente Contrato encontram-se identificados em documento anexo ao contrato,' +
        ' designado por Auto de Aceitação.',
        '1.2. O Cliente escolheu livremente os bens que pretende utilizar, tendo selecionado o Fornecedor por si ' +
        'pretendido, comunicando essa, informação à Candor, e comprometendo-se a pagar os alugueres acordados.']
},
{
    heading: '2. Entrega, instalação dos bens e assistência técnica',
    clauses: [
        '2.1. O Cliente obriga-se a assinar um Auto de Aceitação que consiste numa declaração de confirmação' +
        ' de receção dos bens no momento em que os mesmos lhe forem entregues pelo Fornecedor, estiverem ' +
        'instalados e a funcionar, facto que poderá ser confirmado pela Candor nas instalações do Cliente.',
        '2.2. O Cliente declara que está consciente de que a assistência técnica, a manutenção ou a ' +
        'reparação dos bens é da responsabilidade do Cliente, que tratará diretamente com o Fornecedor ']
}];

  // isHandset holds the breakpoint states for the screens size 
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe([Breakpoints.XSmall]);
  constructor(private breakpointObserver: BreakpointObserver) { }

  // Initailizes the docRecipientId with null
  ngOnDestroy() {
    this.docRecipientId = null;
  }

}
