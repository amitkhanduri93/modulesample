import { ContractSignatureComponent } from './contract-signature.component';
import { ContractSignatureServiceMock } from '../../../../core/mocks/contractSignature.service.mock';
import { throwError, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../../../../core/services/loading.service';
import { MatSnackBar } from '@angular/material';

describe('ContractSignatureComponent', () => {
  let component: ContractSignatureComponent;
  let service;
  let loadingService;
  let activatedRoute: ActivatedRoute;
  let breakpointObserver;
  let router: Router;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    activatedRoute = jasmine.createSpyObj('activatedRoute', ['params']);
    router = jasmine.createSpyObj('router', ['navigate']);
    breakpointObserver = jasmine.createSpyObj('breakpointObserver', ['observe']);
    snackBar = jasmine.createSpyObj('snackBar', ['openFromComponent']);
    service = new ContractSignatureServiceMock();
    loadingService = new LoadingService();
    activatedRoute.params = of({ id: '3' });
    component = new ContractSignatureComponent(activatedRoute,
      loadingService, breakpointObserver, service, router, snackBar);
  });

  it('should create a component ', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('isLoading should be true when getClientSupplierInfo is called  ', () => {
    spyOn(component, 'setLoading');
    component.ngOnInit();
    expect(component.setLoading).toHaveBeenCalled();
  });

  it('isLoading should be false when getContractSignaturePageData is called with error',
    () => {
      spyOn(service, 'getContractSignaturePageData').and.returnValue(throwError(new Error('Error: Failed to fetch data!')));
      component.ngOnInit();
      expect(component.isLoading).toBeFalsy();
    });

  it('isLoading should be false when postContractSignatureView is called with error',
    () => {
      spyOn(service, 'postContractSignatureView').and.returnValue(throwError(new Error('Error: Failed to fetch data!')));
      component.ngOnInit();
      expect(component.isLoading).toBeFalsy();
    });

  it('contractSignature should be null and should unsubcribe all the subscriptions, when ngOnDestroy is called', () => {
    component.routeSubscription = jasmine.createSpyObj('component.routeSubscription', ['unsubscribe']);
    component.contractPageDataSubscription = jasmine.createSpyObj('component.contractPageDataSubscription', ['unsubscribe']);
    component.contractValidationSubscription = jasmine.createSpyObj('component.contractValidationSubscription', ['unsubscribe']);
    component.contractViewSubscription = jasmine.createSpyObj('component.contractViewSubscription', ['unsubscribe']);
    component.contractSignatureIbanDeclarationSubscription =
    jasmine.createSpyObj('component.contractSignatureIbanDeclarationSubscription', ['unsubscribe']);
    component.contractSignatureSignedDataSubscription =
    jasmine.createSpyObj('component.contractSignatureSignedDataSubscription', ['unsubscribe']);
    component.ngOnDestroy();
    expect(component.contractSignature).toBeNull();
    expect(component.routeSubscription.unsubscribe).toHaveBeenCalled();
    expect(component.contractPageDataSubscription.unsubscribe).toHaveBeenCalled();
    expect(component.contractValidationSubscription.unsubscribe).toHaveBeenCalled();
    expect(component.contractViewSubscription.unsubscribe).toHaveBeenCalled();
    expect(component.contractSignatureIbanDeclarationSubscription.unsubscribe).toHaveBeenCalled();
    expect(component.contractSignatureSignedDataSubscription.unsubscribe).toHaveBeenCalled();
  });

});
