import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContractSignature } from '../../../../shared/models/contract-signature';
import { LoadingService } from '../../../../core/services/loading.service';
import { Observable, Subscription } from 'rxjs';
import { BreakpointState, BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ContractSignatureService } from '../../../../core/http/contract-signature.service';
import { MatSnackBar } from '@angular/material';
import { AlertBarComponent } from '../../../../shared/components/alert-bar/alert-bar.component';

@Component({
  selector: 'app-contract-signature',
  templateUrl: './contract-signature.component.html',
  styleUrls: ['./contract-signature.component.scss']
})
export class ContractSignatureComponent implements OnInit, OnDestroy {

  contractSignature: ContractSignature;
  isLoading = false;
  docRecipientId: string;
  token: string;
  contractValidationSubscription: Subscription;
  contractPageDataSubscription: Subscription;
  contractViewSubscription: Subscription;
  routeSubscription: Subscription;
  signedInfo = {
    reason: false
  };
  ibanDeclaration = false;
  contractSignatureSignedDataSubscription: Subscription;
  contractSignatureIbanDeclarationSubscription: Subscription;

  // isHandset and isMobile holds the breakpoint states for the screens size 
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe([Breakpoints.XSmall, Breakpoints.Small]);
  isMobile: Observable<BreakpointState> = this.breakpointObserver.observe([Breakpoints.XSmall]);

  constructor(private activatedRoute: ActivatedRoute,
    private loadingService: LoadingService,
    private breakpointObserver: BreakpointObserver,
    private signatureService: ContractSignatureService,
    private router: Router,
    private snackBar: MatSnackBar) {
  }

  // Initializes the docRecipientId and token from the route params, checks if contract is valid or not,
  // if contract is valid then it gets the contract signature data and also post the contract signature view 
  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe(params => {
      this.docRecipientId = params['id'];
      this.token = params['token'];
    });
    if (this.signatureService.signedData) {
      this.contractSignatureSignedDataSubscription = this.signatureService.signedData.subscribe(data => {
        this.signedInfo = data;
      });
    }
    if (this.signatureService.changeIban) {
      this.contractSignatureIbanDeclarationSubscription = this.signatureService.changeIban.subscribe(data => {
        this.ibanDeclaration = data;
      });
    }
    this.setLoading();
    this.contractValidationSubscription = this.signatureService.getContractValidation(this.docRecipientId).subscribe(data => {
      this.contractPageDataSubscription = this.signatureService.getContractSignaturePageData(this.docRecipientId).subscribe(response => {
        this.contractSignature = response;
        this.loadingService.increment(100);
        this.isLoading = false;
      },
        (reason) => {
          if (reason.status === 400) {
            this.snackBar.openFromComponent(AlertBarComponent, {
              data: 'Ocurreu um erro ao obter a informação do contrato. Por favor, tente novamente mais tarde.',
              panelClass: 'error-snackbar'
            });
          } else if (reason.status === 403) {
            this.snackBar.openFromComponent(AlertBarComponent, {
              data: 'Ocurreu um erro ao obter a informação do contrato. Por favor, tente novamente mais tarde.',
              panelClass: 'error-snackbar'
            });
          } else {
            this.snackBar.openFromComponent(AlertBarComponent, {
              data: 'Ocurreu um erro ao obter a informação do contrato. Por favor, tente novamente mais tarde.',
              panelClass: 'error-snackbar'
            });
          }
          this.loadingService.increment(100);
          this.isLoading = false;
        });
    },
      (reason) => {
        if (reason.status === 400) {
          this.snackBar.openFromComponent(AlertBarComponent, {
            data: 'Ocurreu um erro ao validar o contrato. Por favor, tente novamente mais tarde.',
            panelClass: 'error-snackbar'
          });
        } else if (reason.status === 403) {
          if (reason.error.results.errors[0].message === 'Forbidden - This recipient has already signed!') {
            this.router.navigate(['/contract-signature/invalid/contract/already-signed']);
          } else {
            this.router.navigate(['/contract-signature/invalid/contract/expired']);
          }
        } else {
          this.snackBar.openFromComponent(AlertBarComponent, {
            data: reason.message + ':' + reason.error.results.errors[0].message,
            panelClass: 'error-snackbar'
          });
        }
        this.loadingService.increment(100);
        this.isLoading = false;
      });
    this.contractViewSubscription = this.signatureService.postContractSignatureView(this.docRecipientId).subscribe(response => { },
      (reason) => {
        if (reason.status === 400) {
          this.snackBar.openFromComponent(AlertBarComponent, {
            data: 'Ocurreu um erro ao visualizar o contrato. Por favor, tente novamente mais tarde.',
            panelClass: 'error-snackbar'
          });
        } else {
          this.snackBar.openFromComponent(AlertBarComponent, {
            data: 'Ocurreu um erro ao visualizar o contrato. Por favor, tente novamente mais tarde.',
            panelClass: 'error-snackbar'
          });
        }
        this.loadingService.increment(100);
        this.isLoading = false;
      });
  }

  // Initializes the isLoading to true when it is called 
  setLoading() {
    this.isLoading = true;
  }

  // Unsubscribes all the subscriptions made in the component
  ngOnDestroy() {
    this.contractSignature = null;
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
    if (this.contractValidationSubscription) {
      this.contractValidationSubscription.unsubscribe();
    }
    if (this.contractPageDataSubscription) {
      this.contractPageDataSubscription.unsubscribe();
    }
    if (this.contractViewSubscription) {
      this.contractViewSubscription.unsubscribe();
    }
    if (this.contractSignatureIbanDeclarationSubscription) {
      this.contractSignatureIbanDeclarationSubscription.unsubscribe();
    }
    if (this.contractSignatureSignedDataSubscription) {
      this.contractSignatureSignedDataSubscription.unsubscribe();
    }
  }
}
